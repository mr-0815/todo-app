'use strict';

async function deleteAndUpdateList(id) {
  buttonChangeTodo.removeEventListener('click', async () => {
    await deleteAndUpdateList(id);
  });
  const response = await deleteTodo(id);
  if (response && response.status === 200) {
    displayAlert('Todo deleted.');
  } else if (response) {
    displayAlert('Something went wrong.', 'warn');
  }
  closeModal('edit');
}

async function deleteTodo(id) {
  return await fetchWithTimeout(
    new URL(`delete-todo/${id}`, baseUrl),
    'POST',
    JSON.stringify({ description: null })
  );
}
