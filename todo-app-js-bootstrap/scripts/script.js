'use strict';

// frequently used variables
const tableContainer = document.getElementById('table-container');

// const baseUrl = new URL('http://localhost:3000/');
// const baseUrl = new URL('http://mr-macbook.local:3000/');
// const baseUrl = new URL('http://192.168.0.157:3000/');
let baseUrl;
(async () => {
  baseUrl = await getStore();
})();

async function getStore() {
  const local = localStorage.getItem('store');

  const fallback =
    `${location.protocol}//${location.hostname}:3000` ||
    'http://localhost:3000/';

  const store = local ? new URL(local) : new URL(fallback);

  return store;
}
