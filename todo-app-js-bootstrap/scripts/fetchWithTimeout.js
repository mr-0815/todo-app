'use strict';

async function fetchWithTimeout(
  url,
  method = 'GET',
  body = {},
  timeout = 1000
) {
  var controller = new AbortController();
  var signal = controller.signal;
  setTimeout(() => {
    controller.abort('timeout');
  }, timeout);
  try {
    if (method === 'GET') {
      const response = await fetch(url, {
        signal,
      });
      return response;
    } else if (method === 'POST') {
      const response = await fetch(url, {
        signal,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body,
      });
      return response;
    } else {
      return new Error('Invalid method called');
    }
  } catch {
    displayAlert('Server not reachable.', 'warn');
  }
}
