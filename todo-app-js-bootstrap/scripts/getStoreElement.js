'use strict';

async function getStoreElement(id) {
  const response = await fetchWithTimeout(new URL(`get-item/${id}`, baseUrl));

  if (response) {
    const data = await response.json();
    return data;
  } else {
    return;
  }
}
