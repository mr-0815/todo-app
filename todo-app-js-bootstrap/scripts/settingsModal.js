'use strict';

const formContainerSettingStore = document.getElementById(
  'formContainerSettingStore'
);
formContainerSettingStore.addEventListener('submit', async (event) => {
  event.preventDefault();
  await setStore();
});

function showSettingsModal() {
  // show modal
  const settingsModal = document.getElementById('settings-modal');
  const bsModalSettings = bootstrap.Modal.getOrCreateInstance(settingsModal);
  bsModalSettings.show();

  // configure submit of port setting
  document.getElementById('storeSettingInputField').value = baseUrl;
}

// set store
async function setStore() {
  const input = document.getElementById('storeSettingInputField').value;
  const chosenURL = new URL(input);

  // test if valid

  const response = await fetchWithTimeout(chosenURL);
  if (response && response.status === 200) {
    baseUrl = chosenURL;

    // save to localStorage
    localStorage.setItem('store', chosenURL);

    // close modal
    bootstrap.Modal.getOrCreateInstance(
      document.getElementById('settings-modal')
    ).hide();

    // success alert
    displayAlert(`Store changed to ${chosenURL}`);

    // close 'disconnected' alert, if present
    const alertDangerEl = document.getElementById('alert-danger');
    if (
      document.getElementById('alert-container').innerText &&
      alertDangerEl &&
      alertDangerEl.classList.contains('switchFromStoreSetting')
    ) {
      bootstrap.Alert.getInstance(alertDangerEl).close();
      alertDangerEl.classList.remove('switchFromStoreSetting');
    }
  } else {
    displayAlert(`${chosenURL} not reachable.`, 'warn');
    alertDangerEl.classList.add('switchFromStoreSetting');
    return false;
  }
}
