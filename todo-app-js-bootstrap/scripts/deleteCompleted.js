'use strict';

const buttonDeleteCompleted = document.getElementById('buttonDeleteCompleted');
buttonDeleteCompleted.addEventListener('click', async (event) => {
  event.preventDefault();
  const response = await deleteCompleted();
  if (response) {
    const { deletedCount } = response;
    if (deletedCount) {
      displayAlert(
        `${deletedCount} "done" item${deletedCount !== 1 ? 's' : ''} deleted.`
      );
    } else if (deletedCount === 0) {
      displayAlert('No "done" items to delete.', 'info', 5000);
    }
  }
  closeModal('settings');
});

async function deleteCompleted() {
  const url = new URL('delete-completed', baseUrl);
  const response = await fetchWithTimeout(
    url,
    'POST',
    JSON.stringify({ description: null })
  );
  if (response && response.status === 200) {
    const { deletedCount } = await response.json();
    return { deletedCount };
  } else {
    return;
  }
}
