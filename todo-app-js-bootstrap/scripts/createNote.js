'use strict';

const buttonNoteTodo = document.getElementById('buttonNoteTodo');
const description = document.getElementById('todo-description');

buttonNoteTodo.addEventListener('click', async (event) => {
  event.preventDefault();
  if (!description.value || description.value.trim().length == 0) {
    displayAlert('No subject entered.');
  } else {
    await noteTodo(baseUrl + 'note-todo', description.value.trim());
    description.value = '';
  }
});

async function noteTodo(url, description) {
  const data = { description };

  try {
    const response = await fetchWithTimeout(url, 'POST', JSON.stringify(data));
    if (response.status === 201) {
      displayAlert(`Todo "${description}" added.`);
    } else {
      displayAlert('Todo could not be added.', 'warn');
    }
  } catch {
    displayAlert('Server not reachable.', 'warn');
    return;
  }
}
