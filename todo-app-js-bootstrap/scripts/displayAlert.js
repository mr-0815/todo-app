'use strict';

function displayAlert(text, level = 'info', timeout = 2000) {
  if (level === 'warn') {
    const alertNode = document.getElementById('alert-danger');
    // just blink, if already present
    alertNode ? alertBlink(alertNode) : displayDanger();
  } else {
    // level === 'info'
    displayInfo(timeout);
  }

  function displayInfo() {
    // create node
    const containerEl = document.getElementById('alert-container');
    const div = document.createElement('div');
    div.innerHTML = `
    <i class="fa fa-circle-info bi flex-shrink-0 me-2" style="font-style:normal"></i>
    <span>
    ${text} 
    </span>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    `;
    const newDiv = containerEl.appendChild(div);
    [
      'alert',
      'alert-primary',
      'alert-dismissible',
      'fade',
      'show',
      'd-flex',
      'align-items-center',
    ].forEach((tag) => {
      newDiv.classList.add(tag);
    });
    newDiv.id = 'alert-info';
    newDiv.role = 'alert';

    // create alert
    const alertNode = document.getElementById('alert-info');
    new bootstrap.Alert(alertNode);

    // delete after delay
    setTimeout(() => {
      const alert = bootstrap.Alert.getInstance(alertNode);
      alert.close();
    }, timeout);
  }

  function displayDanger() {
    // create node
    const containerEl = document.getElementById('alert-container');
    const div = document.createElement('div');
    div.innerHTML = `
    <i class="fa fa-warning bi flex-shrink-0 me-2" style="font-style:normal"></i>
    <span>
    ${text} 
    </span>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    `;
    const newDiv = containerEl.appendChild(div);
    [
      'alert',
      'alert-danger',
      'alert-dismissible',
      'fade',
      'show',
      'd-flex',
      'align-items-center',
    ].forEach((tag) => {
      newDiv.classList.add(tag);
    });
    newDiv.id = 'alert-danger';
    newDiv.role = 'alert';

    // create alert
    const alertNode = document.getElementById('alert-danger');
    new bootstrap.Alert(alertNode);
  }
}

function alertBlink(alertNode) {
  alertNode.style = 'visibility: hidden;';
  setTimeout(() => {
    alertNode.style = 'visibility: initial;';
  }, 50);
}
