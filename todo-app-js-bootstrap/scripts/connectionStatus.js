'use strict';

// setup repeating check at startup
testConnection();
setInterval(() => {
  testConnection();
}, 60_000);

const connectionStatusButton = document.getElementById(
  'checkConnectionManually'
);
connectionStatusButton.addEventListener('click', async (event) => {
  event.preventDefault();
  const connected = await testConnection();
  connected && displayAlert('Connected');
});

async function testConnection(timeout = 1000) {
  const response = await fetchWithTimeout(baseUrl);
  if (response && response.status === 200) {
    showConnected();
    return true;
  } else {
    showDisconnected();
    return false;
  }
}

function showDisconnected() {
  const statusElem = document.getElementById('connectionStatus');
  statusElem.innerText = 'Disconnected';
  statusElem.classList.remove('text-primary');
  statusElem.classList.add('text-danger');
}

function showConnected() {
  const statusElem = document.getElementById('connectionStatus');
  statusElem.innerText = 'Connected';
  statusElem.classList.remove('text-danger');
  statusElem.classList.add('text-primary');

  // close 'disconnected' alert, if present
  if (
    document.getElementById('alert-container').innerText &&
    document.getElementById('alert-danger')
  ) {
    bootstrap.Alert.getInstance(
      document.getElementById('alert-danger')
    ).close();
  }
}
