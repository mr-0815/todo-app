'use strict';

const buttonFetch = document.getElementById('buttonFetch');
buttonFetch.addEventListener('click', (event) => {
  event.preventDefault();
  showTodos('remaining');
});

const dropdownFetch = document.getElementById('dropdownFetch');
dropdownFetch.addEventListener('click', (event) => {
  event.preventDefault();
  showTodos('remaining');
});

const dropdownFetchAll = document.getElementById('dropdownFetchAll');
dropdownFetchAll.addEventListener('click', (event) => {
  event.preventDefault();
  showTodos('all');
});

async function showTodos(mode = 'remaining') {
  let url;
  if (mode === 'remaining') {
    url = new URL('todos', baseUrl);
  } else if (mode === 'all') {
    url = new URL('get-all-todos', baseUrl);
  } else {
    throw new Error('mode invalid');
  }
  const response = await fetchWithTimeout(url);
  if (response) {
    const data = await response.json();
    if (data.length == 0) {
      displayAlert('Your list is empty.');
    } else {
      tableContainer.classList.remove('remaining', 'all');
      mode === 'remaining'
        ? tableContainer.classList.add('remaining')
        : tableContainer.classList.add('all');
      displayModal(data);
    }
  }
}
