'use strict';

const slider = document.getElementById('slider');

if (localStorage.getItem('theme') === 'theme-dark') {
  setTheme('theme-dark');
  slider.checked = false;
}

document.getElementById('themeSwitcher').addEventListener('click', (event) => {
  event.preventDefault();
  slider.checked = !slider.checked;
  toggleTheme();
});

function setTheme(themeName) {
  localStorage.setItem('theme', themeName);
  const css = document.getElementById('dark-mode-css');
  if (themeName === 'theme-dark') {
    css.disabled = false;
    document.querySelector('body').classList.add('dark');
  } else {
    css.disabled = true;
    document.querySelector('body').classList.remove('dark');
  }
}

function toggleTheme() {
  if (localStorage.getItem('theme') === 'theme-dark') {
    setTheme('theme-light');
  } else {
    setTheme('theme-dark');
  }
}
