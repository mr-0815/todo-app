'use strict';

async function changeTodo(todo) {
  const { id } = todo;

  const response = await fetchWithTimeout(
    new URL(`edit-todo/${id}`, baseUrl),
    'POST',
    JSON.stringify(todo)
  );
  if (response && response.status === 200) {
    displayAlert('Todo changed.');
  } else {
    closeModal('edit');
  }
}
