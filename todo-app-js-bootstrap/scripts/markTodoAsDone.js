'use strict';

async function markTodoAsDone(id) {
  const response = await fetchWithTimeout(
    new URL(`mark-todo-as-done/${id}`, baseUrl),
    'POST',
    JSON.stringify({ description: null })
  );
  response ? updateTodoList(response, id) : closeModal('tasks');
}

function updateTodoList(response, id) {
  if (response.status === 200) {
    // update list "remaining" by popping item
    if (tableContainer.classList.contains('remaining')) {
      document.getElementById(id).remove();
      // update counter
      const counterEl = document.getElementById('tableCaptionItems');
      const prevCount = parseInt(counterEl.querySelector('span').innerText);
      if (prevCount >= 2) {
        counterEl.innerHTML = `<span>${prevCount - 1}</span> Item${
          prevCount !== 2 ? 's' : ''
        }`;
      } else {
        closeModal('tasks');
        displayAlert('No more todos.');
      }
      // show updated list "all"
    } else {
      clearAllItemsInTableContainer();
      showTodos('all');
    }
  } else {
    displayAlert('Todo could not be changed.');
  }
}
