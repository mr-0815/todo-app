'use strict';

async function displayEditTodo(id) {
  // hide previous modal
  closeModal('tasks');

  // show edit modal
  const bsModalEdit = bootstrap.Modal.getOrCreateInstance(
    document.getElementById('edit-modal')
  );
  bsModalEdit.show();

  // prepopulate info
  const elem = await getStoreElement(id);
  if (!elem) {
    // close edit modal & exit
    closeModal('edit');
    throw new Error('offline');
  }

  const { description, status, additionalInfo, created, lastModified } =
    elem[0];

  document.getElementById(
    'edit-task-headline'
  ).innerText = `Edit Task "${description}"`;

  document.getElementById('todo-description-edit').placeholder = description;
  document.getElementById('todo-description-edit').value = description;
  document.getElementById('todo-description-edit').focus();

  document.querySelector(
    `#edit-modal select[name="todo-status"] option[value=${status}]`
  ).selected = true;

  document.getElementById('additionalInfo').placeholder = additionalInfo
    ? additionalInfo
    : 'Additional Information';
  document.getElementById('additionalInfo').value = additionalInfo;

  document.getElementById('showInfoId').innerText = `Id: ${id}`;
  document.getElementById('showInfoCreated').innerText = `Created: ${new Date(
    created
  ).toLocaleString()}`;
  document.getElementById(
    'showInfoLastModified'
  ).innerText = `Last Modified: ${new Date(lastModified).toLocaleString()}`;

  // apply changes
  const buttonChangeTodo = document.getElementById('buttonChangeTodo');
  buttonChangeTodo.onclick = async (event) => {
    event.preventDefault();
    await applyChanges();
    bsModalEdit.hide();
    cleanUpEditFields();
  };

  async function applyChanges() {
    const todo = {
      id,
      description: document.getElementById('todo-description-edit').value,
      status: document.querySelector('select[name="todo-status"]').value,
      additionalInfo: document.getElementById('additionalInfo').value,
    };
    await changeTodo(todo);
    bsModalEdit.hide();
  }

  function cleanUpEditFields() {
    document.getElementById('edit-task-headline').innerText = `Edit Task`;
    document.getElementById('todo-description-edit').placeholder = '';
    document
      .querySelectorAll(`#edit-modal select[name="todo-status"]`)
      .forEach((node) => {
        node.selected = false;
      });
    document.getElementById('additionalInfo').placeholder = '';
  }
}

// delete button
const deleteButton = document.getElementById('buttonDeleteTodo');
deleteButton.addEventListener('click', async (event) => {
  event.preventDefault();
  await deleteAndUpdateList(id);
});
