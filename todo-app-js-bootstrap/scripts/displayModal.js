'use strict';

function displayModal(resultData) {
  // set up modal
  const tasksModal = document.getElementById('tasks-modal');
  const bsModalTodos = bootstrap.Modal.getOrCreateInstance(tasksModal);
  bsModalTodos.show();
  // cleanup when modal closed
  tasksModal.addEventListener('hidden.bs.modal', () => {
    clearAllItemsInTableContainer();
  });

  // create table
  displayAsTable(resultData, tableContainer);

  // update counter
  const innerHTML = `<span>${resultData.length}</span> Item${
    resultData.length !== 1 ? 's' : ''
  }`;
  document.getElementById('tableCaptionItems').innerHTML = innerHTML;
}

function displayAsTable(resultData, tableContainer) {
  const showAll = tableContainer.classList.contains('all') ? true : false;

  for (let i = 0; i < resultData.length; i++) {
    if (resultData[i].status == 'done' && !showAll) {
      return;
    }

    const tr = createChildText(tableContainer, 'tr', '');
    tr.id = resultData[i].id;

    // checkbox
    const checkboxHTML = `
      <input class="form-check-input" type="checkbox">`;
    const checkboxParentEl = createChildText(tr, 'td', checkboxHTML);
    const checkboxEl = checkboxParentEl.querySelector('input');
    if (resultData[i].status === 'done') {
      checkboxEl.checked = true;
      checkboxEl.addEventListener('click', (event) => {
        event.preventDefault();
        event.target.checked = true;
        displayEditTodo(event.target.parentElement.parentElement.id);
      });
    } else {
      checkboxEl.addEventListener('click', (event) => {
        event.preventDefault();
        markTodoAsDone(event.target.closest('tr').id);
      });
    }

    // description
    let descriptionText = resultData[i].description.slice(0, 25);
    if (resultData[i].description.length > 25) {
      descriptionText += '[…]';
    }
    createChildText(tr, 'td', descriptionText);

    // status
    const statusHTML = `
      <span class="badge rounded-pill text-nowrap"></span>`;
    const statusParentEl = createChildText(tr, 'td', statusHTML);
    const statusEl = statusParentEl.querySelector('span');

    switch (resultData[i].status) {
      case 'open':
        statusEl.classList.add('bg-primary');
        statusEl.innerText = 'open';
        break;

      case 'in Progress':
        statusEl.classList.add('bg-success');
        statusEl.innerText = 'in Progress';
        break;

      case 'done':
        statusEl.classList.add('bg-secondary');
        statusEl.innerText = 'done';
        break;

      default:
        statusEl.classList.add('bg-info');
        statusEl.innerText = resultData[i].status;
        break;
    }

    // edit
    const editHTML = `
      <span class="badge btn bg-light text-muted">edit</span>
    `;
    const editParentEl = createChildText(tr, 'td', editHTML);
    const editEl = editParentEl.querySelector('span');
    editEl.onclick = function (event) {
      event.preventDefault();
      displayEditTodo(event.target.parentElement.parentElement.id);
    };
    // edit also by clicking anywhere in the line
    tr.addEventListener('click', (event) => {
      event.preventDefault();
      !event.target.classList.contains('form-check-input') &&
        displayEditTodo(event.target.closest('tr').id);
    });

    // styling
    if (resultData[i].status == 'done') {
      if (document.querySelector('body').classList.contains('dark')) {
        tr.classList.add('table-dark');
      } else {
        tr.classList.add('text-muted', 'table-secondary');
      }
    }
  }

  function createChildText(parent, type, html) {
    const elem = document.createElement(type);
    elem.innerHTML = html;
    parent.appendChild(elem);
    return elem;
  }
}

function clearAllItemsInTableContainer() {
  document.querySelectorAll('#table-container>*').forEach((el) => el.remove());
}

function closeModal(type) {
  // type === 'edit'|'tasks'
  const modal = bootstrap.Modal.getInstance(
    document.getElementById(`${type}-modal`)
  );
  modal && modal.hide();
}
