# Todo-App ddd

> thenativeweb.io

## Environment Variables:

- `STORE`: 'mongo' (default), 'local'
- `PORT`: 3000 (default)

## Startup

- start Docker Desktop
- `npm run mongo`
  - as a shorthand for:
  - `npm run start-mongodb`
  - `node start app.js`
- using local storage (RAM) instead:
  - `STORE='local' node app.js`

## Clear

- `npm run stop-mongodb`
- quit Docker Desktop

# Routes

## Queries

- `GET '/todos'`
  - get remaining todos
- `GET '/get-all-todos'`
  - get all todos in database
- `GET '/get-item/:id'`
  - get specific item
- `GET '/empty-list'`
  - delete all items in store

## Commands

- `POST '/note-todo'`
- `POST '/mark-todo-as-done/:id'`
- `POST '/edit-todo/:id'`
- `POST '/delete-todo/:id'`
- `POST '/delete-completed'`

# JSON Body Elements

```js
{
  id: randomUUID (auto),
  description: input,
  status: 'open' | 'in Progress' | 'done' | any,
  additionalInfo,
  created: auto,
  lastModified: auto
}
```
