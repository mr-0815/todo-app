#!/usr/bin/env node
'use strict';

const { flaschenpost } = require('flaschenpost');
const { getApi } = require('./lib/getApi');
const http = require('http');
const { InMemoryStore } = require('./lib/store/InMemoryStore');
const { MongoDbStore } = require('./lib/store/MongoDbStore');
const { processenv } = require('processenv');

(async () => {
  const logger = flaschenpost.getLogger();

  // Store wählen:
  const storeEnv = processenv('STORE', 'mongo');
  let store;

  logger.info('Store: ', { storeEnv });

  switch (storeEnv) {
    case 'local':
      store = new InMemoryStore();
      break;

    case 'mongo':
      store = new MongoDbStore({
        hostname: 'localhost',
        port: 27_017,
        database: 'todos',
        username: 'node',
        password: 'node'
      });
      break;

    default:
      logger.fatal(`No store '${storeEnv}' found.`);
      process.exit(1);
  }

  await store.initialize();

  const port = processenv('PORT', 3_000);

  const api = getApi({ store });

  const server = http.createServer(api);

  server.listen(port, () => {
    logger.info('Server started', { port });
  });
})();
