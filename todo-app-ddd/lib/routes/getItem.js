'use strict';

const getItem = function ({ store }) {
  return async function (req, res) {
    const { id } = req.params;

    const item = await store.getItem({ id });

    if (item === 500) {
      res.status(500).json({ message: 'Database not reachable' });
    } else {
      res.json(item);
    }
  };
};

module.exports = { getItem };
