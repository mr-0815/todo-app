'use strict';

const getTodos = function ({ store }) {
  return async function (req, res) {
    const remainingTodos = await store.getRemainingTodos();

    if (remainingTodos === 500) {
      res.status(500).json({ message: 'Database not reachable' });
    } else {
      res.json(remainingTodos);
    }
  };
};

module.exports = { getTodos };
