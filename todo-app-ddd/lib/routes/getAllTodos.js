'use strict';

const getAllTodos = function ({ store }) {
  return async function (req, res) {
    const allTodos = await store.getAllTodos();

    if (allTodos === 500) {
      // Status Code: 500 Internal Server Error
      res.status(500).json({ message: 'Database not reachable' });
    } else {
      res.json(allTodos);
    }
  };
};

module.exports = { getAllTodos };
