'use strict';

const emptyList = function ({ store }) {
  return async function (req, res) {
    await store.emptyList();
    res.json({});
  };
};

module.exports = { emptyList };
