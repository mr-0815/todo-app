'use strict';

const deleteTodo = function ({ store }) {
  return async function (req, res) {
    const { id } = req.params;

    try {
      await store.deleteTodo({ id });
    } catch {
      return res.status(404).json({}).end();
    }

    res.json({});
  };
};

module.exports = { deleteTodo };
