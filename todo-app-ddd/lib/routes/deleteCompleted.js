'use strict';

const deleteCompleted = function ({ store }) {
  return async function (req, res) {
    try {
      const deletion = await store.deleteCompleted();
      const { deletedCount } = deletion;

      res.json({ deletedCount });
    } catch {
      res.status(500).json({});
    }
  };
};

module.exports = { deleteCompleted };
