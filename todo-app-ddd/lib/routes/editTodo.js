'use strict';

const { changeTodo } = require('../changeTodo');

const editTodo = function ({ store }) {
  return async function (req, res) {
    const { id } = req.params;
    const { description, status, additionalInfo } = req.body;
    const todo = changeTodo({ id, description, status, additionalInfo });

    try {
      await store.editTodo({ todo });
      res.json(todo);
    } catch {
      return res.status(404).end();
    }
  };
};

module.exports = { editTodo };
