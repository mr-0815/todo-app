'use strict';

const { createTodo } = require('../createTodo');

const noteTodo = function ({ store }) {
  return async function (req, res) {
    const { description } = req.body;
    const { additionalInfo } = req.body;
    const todo = createTodo({ description, additionalInfo });

    const change = await store.noteTodo({ todo });

    if (change === 500) {
      res.status(500).json({});
    } else {
      res.status(201).json({
        id: todo.id,
        description,
        additionalInfo
      });
    }
  };
};

module.exports = { noteTodo };
