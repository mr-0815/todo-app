'use strict';

const { MongoClient } = require('mongodb');
const { flaschenpost } = require('flaschenpost');

const logger = flaschenpost.getLogger();

class MongoDbStore {
  constructor ({ hostname, port, username, password, database }) {
    this.hostname = hostname;
    this.port = port;
    this.username = username;
    this.password = password;
    this.database = database;
  }

  async initialize () {
    const connectionString = `mongodb://${this.username}:${this.password}@${this.hostname}:${this.port}/`;
    const client = new MongoClient(connectionString);

    try {
      await client.connect();
    } catch {
      logger.fatal('Database not reachable');
      throw new Error('Database not reachable');
    }

    const database = client.db(this.database);
    const collection = database.collection('todos');

    this.collection = collection;
  }

  async noteTodo ({ todo }) {
    try {
      await this.collection.insertOne(todo);
    } catch {
      logger.error('Database not reachable');

      return 500;
    }
  }

  async markTodoAsDone ({ id }) {
    const { modifiedCount } = await this.collection.updateOne(
      { id },
      {
        $set: { status: 'done' },
        $currentDate: { lastModified: true }
      }
    );

    if (modifiedCount === 0) {
      logger.warn('Todo not found.');
    }
  }

  async editTodo ({ todo }) {
    const { id, description, status, additionalInfo } = todo;
    const { modifiedCount } = await this.collection.updateOne(
      { id },
      {
        $set: {
          description,
          status,
          additionalInfo
        },
        $currentDate: { lastModified: true }
      }
    );

    if (modifiedCount === 0) {
      logger.warn('Todo not found.');
    }
  }

  async getAllTodos () {
    try {
      return await this.collection.find(
        {},
        { projection: { _id: 0 }}
      ).toArray();
    } catch {
      logger.error('Database not reachable');

      return 500;
    }
  }

  async getRemainingTodos () {
    try {
      return await this.collection.find(
        { status: { $ne: 'done' }},
        { projection: { _id: 0 }}
      ).toArray();
    } catch {
      logger.error('Database not reachable');

      return 500;
    }
  }

  async getItem ({ id }) {
    try {
      return await this.collection.find(
        { id },
        { projection: { _id: 0 }}
      ).toArray();
    } catch {
      logger.error('Database not reachable');

      return 500;
    }
  }

  async deleteTodo ({ id }) {
    const { modifiedCount } = await this.collection.deleteOne(
      { id }
    );

    if (modifiedCount === 0) {
      logger.warn('Todo not found.');
    }
  }

  async deleteCompleted () {
    const { deletedCount } = await this.collection.deleteMany(
      { status: 'done' }
    );

    return { deletedCount };
  }

  async emptyList () {
    await this.collection.deleteMany({});
  }
}

module.exports = { MongoDbStore };
