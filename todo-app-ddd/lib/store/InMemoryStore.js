'use strict';

class InMemoryStore {
  async initialize () {
    this.todos = [];
  }

  async noteTodo ({ todo }) {
    this.todos.push(todo);
  }

  async markTodoAsDone ({ id }) {
    const todo = this.todos.find(item => item.id === id);

    if (!todo) {
      throw new Error('Todo not found.');
    }

    todo.status = 'done';
  }

  async deleteTodo ({ id }) {
    const todo = this.todos.find(item => item.id === id);

    if (!todo) {
      throw new Error('Todo not found.');
    }

    this.todos = this.todos.splice(this.todos.indexOf(todo) - 1, 1);
  }

  async deleteCompleted () {
    const todoToBeChanged = this.todos.filter(item => item.status === 'todo');

    todoToBeChanged.forEach(item => {
      this.todos.splice(this.todos.indexOf(item) - 1, 1);
    });

    const deletedCount = todoToBeChanged.length;

    return { deletedCount };
  }

  async editTodo ({ todo }) {
    const { id, description, status } = todo;
    const todoToBeChanged = this.todos.find(item => item.id === id);

    if (!todoToBeChanged) {
      throw new Error('Todo not found.');
    }

    todoToBeChanged.description = description;
    todoToBeChanged.status = status;
  }

  async getAllTodos () {
    return this.todos;
  }

  async getItem ({ id }) {
    const foundTodo = this.todos.find(item => item.id === id);

    return { foundTodo };
  }

  async getRemainingTodos () {
    return this.todos.filter(item => item.status === 'open');
  }

  async emptyList () {
    this.todos = [];
  }
}

module.exports = { InMemoryStore };
