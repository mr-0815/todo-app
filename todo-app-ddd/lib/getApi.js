'use strict';

const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const { deleteCompleted } = require('./routes/deleteCompleted');
const { deleteTodo } = require('./routes/deleteTodo');
const { editTodo } = require('./routes/editTodo');
const { emptyList } = require('./routes/emptyList');
const { getAllTodos } = require('./routes/getAllTodos');
const { getItem } = require('./routes/getItem');
const { getTodos } = require('./routes/getTodos');
const { markTodoAsDone } = require('./routes/markTodoAsDone');
const { noteTodo } = require('./routes/noteTodo');

const getApi = function ({ store }) {
  const api = express();

  api.use(cors());
  api.use(bodyParser.json());

  // Commands

  api.post('/note-todo', noteTodo({ store }));

  api.post('/mark-todo-as-done/:id', markTodoAsDone({ store }));

  api.post('/edit-todo/:id', editTodo({ store }));

  api.post('/delete-todo/:id', deleteTodo({ store }));

  api.post('/delete-completed', deleteCompleted({ store }));

  // Queries

  api.get('/', (req, res) => {
    res.json({
      message: 'Welcome to todo-ddd-API.'
    });
  });

  api.get('/todos', getTodos({ store }));

  api.get('/get-all-todos', getAllTodos({ store }));

  api.get('/get-item/:id', getItem({ store }));

  // Helpers

  // delete all items in store
  api.get('/empty-list', emptyList({ store }));

  api.get('*', (req, res) => {
    res.status(404).end();
  });

  return api;
};

module.exports = { getApi };
