'use strict';

const changeTodo = function ({ id, description, status, additionalInfo }) {
  return {
    id,
    description,
    status,
    additionalInfo
  };
};

module.exports = { changeTodo };
