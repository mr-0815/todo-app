'use strict';

const crypto = require('crypto');

const createTodo = function ({ description, additionalInfo = '' }) {
  const now = new Date();

  return {
    id: crypto.randomUUID(),
    description,
    status: 'open',
    additionalInfo,
    created: now,
    lastModified: now
  };
};

module.exports = { createTodo };
