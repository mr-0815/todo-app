/* eslint-disable no-undef */
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {},
  },
  darkMode: 'class',
  plugins: [require('daisyui')],
  daisyui: {
    themes: [
      {
        'mr-light': {
          primary: '#DCF235',
          secondary: '#67733D',
          accent: 'red', // ???
          neutral: '#636573',
          'base-100': 'white',
          // 'base-200': '#636573', // in use for table + debugInfo
          info: '#4EF2EF',
          success: '#62F241',
          warning: '#F27229',
          error: '#F21DC0',
        },
      },
      {
        'mr-dark': {
          primary: '#DCF235',
          secondary: '#BFA7F2', // not in use
          accent: 'red', // ???
          neutral: '#636573',
          'base-100': '#323240',
          // 'base-200': '',  // in use for table + debugInfo
          info: '#4EF2EF',
          success: '#87DB25',
          warning: '#F27229',
          error: '#F21DC0',
        },
      },
    ],
  },
};
