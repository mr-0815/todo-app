import React, { useState } from 'react';
import '../Notification.css';
import PropTypes from 'prop-types';

const Notification = (props) => {
  const [exit, setExit] = useState(false);

  const handleCloseNotification = () => {
    setExit(true);
    setTimeout(() => {
      props.dispatch({
        type: 'REMOVE_NOTIFICATION',
        id: props.id,
      });
    }, 400);
  };

  React.useEffect(() => {
    const timeout = setTimeout(() => {
      handleCloseNotification();
    }, 1500);
    return () => clearTimeout(timeout);
  }, []);

  return (
    <div
      className={`alert alert-${
        props.alertType
      } rounded-lg shadow-lg w-full md:w-11/12 md:mx-auto md:p-3 mb-4 notification-item ${
        exit ? 'exit' : ''
      }`}
    >
      <p>{props.message}</p>
    </div>
  );
};

Notification.propTypes = {
  dispatch: PropTypes.func,
  id: PropTypes.string,
  alertType: PropTypes.string,
  message: PropTypes.string,
};

export default Notification;
