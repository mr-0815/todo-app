import React from 'react';
import { useNavigate } from 'react-router-dom';

function IsLoadingSpinner() {
  const navigate = useNavigate();
  return (
    <>
      <div className="block self-center">
        <button className="btn btn-primary loading">Fetching data...</button>
      </div>
      <div className="block self-center mt-3">
        <button
          onClick={() => navigate('/503')}
          className="btn btn-sm btn-ghost"
        >
          Cancel
        </button>
      </div>
    </>
  );
}

export default IsLoadingSpinner;
