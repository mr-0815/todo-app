import React from 'react';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';

function ErrorMessage({ msg }) {
  const navigate = useNavigate();

  return (
    <div className="hero mt-11">
      <div className="hero-content text-center">
        <div className="max-w-md">
          <h1 className="text-5xl font-bold dark:text-primary leading-snug">
            {msg}
          </h1>
          <p className="py-6">
            Provident cupiditate voluptatem et in. Quaerat fugiat ut assumenda
            excepturi exercitationem quasi. In deleniti eaque aut repudiandae et
            a id nisi.
          </p>
          <button
            onClick={() => navigate('/')}
            className="btn btn-primary w-36"
          >
            HOME
          </button>
        </div>
      </div>
    </div>
  );
}

ErrorMessage.propTypes = {
  msg: PropTypes.string,
};

export default ErrorMessage;
