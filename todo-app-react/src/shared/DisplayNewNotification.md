```jsx
import { useNotification } from '../contexts/NotificationContext';

const dispatch = useNotification();
const handleNewNotification = (alertType, message) => {
  dispatch({
    alertType,
    message,
  });
};
```
