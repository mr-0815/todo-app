import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { AppProvider } from './contexts/AppContext';
import { NotificationProvider } from './contexts/NotificationContext';
import { TodoProvider } from './contexts/TodoContext';
import EditTodoPage from './pages/EditTodoPage';
import ErrorPage from './pages/ErrorPage';
import Footer from './components/Footer';
import Header from './components/Header';
import ListTodos from './components/ListTodos';
import NoteTodo from './components/NoteTodo';

function App() {
  return (
    <Router>
      <AppProvider>
        <NotificationProvider>
          <TodoProvider>
            <div className="relative h-screen">
              <Header title={'Todo-App'} />
              <div
                className="flex flex-col absolute mr-container-padding top-16 w-full overflow-auto"
                style={{ maxHeight: 'calc(100vh - 4rem - 2.75rem)' }}
              >
                <Routes>
                  <Route
                    exact
                    path="/"
                    element={
                      <>
                        <NoteTodo />
                        <ListTodos />
                      </>
                    }
                  />
                  <Route path="/edit/:id" element={<EditTodoPage />} />
                  <Route
                    path="/503"
                    element={<ErrorPage msg="503 – Service Unavailable" />}
                  />
                  <Route
                    path="*"
                    element={<ErrorPage msg="404 – Not Found" />}
                  />
                </Routes>
              </div>
              <Footer />
            </div>
          </TodoProvider>
        </NotificationProvider>
      </AppProvider>
    </Router>
  );
}

export default App;
