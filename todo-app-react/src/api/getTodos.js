'use strict';

import { fetchWithTimeout } from './fetchWithTimeout';

export default async function getTodosApi() {
  let result = {};

  const response = await fetchWithTimeout('get-all-todos');
  if (response) {
    const data = await response.json();
    if (data.length == 0) {
      console.info('Your list is empty.');
    } else {
      result = data;
    }
  } else {
    console.error('Server not reachable.');
    // console.error(error?.response?.data?.message);
  }
  return result;
}
