'use strict';

import { baseUrl } from './api';

export async function fetchWithTimeout(
  urlPath,
  method = 'GET',
  body = {},
  timeout = 5000
) {
  const url = new URL(urlPath, baseUrl);
  var controller = new AbortController();
  var signal = controller.signal;
  setTimeout(() => {
    controller.abort('timeout');
  }, timeout);
  try {
    if (method === 'GET') {
      const response = await fetch(url, {
        signal,
      });
      return response;
    } else if (method === 'POST') {
      const response = await fetch(url, {
        signal,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body,
      });
      return response;
    } else {
      return new Error('Invalid method called');
    }
  } catch {
    throw new Error('Server not reachable.');
  }
}
