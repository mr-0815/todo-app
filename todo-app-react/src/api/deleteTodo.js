'use strict';

import { fetchWithTimeout } from './fetchWithTimeout';

export default async function deleteTodoApi(id) {
  const response = await fetchWithTimeout(
    `delete-todo/${id}`,
    'POST',
    JSON.stringify({ description: null })
  );

  if (response && response.status === 200) {
    console.info('Todo deleted.');
  } else if (response) {
    console.error('Something went wrong.');
  }

  return response.status;
}
