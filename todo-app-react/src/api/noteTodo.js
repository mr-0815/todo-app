'use strict';

import { fetchWithTimeout } from './fetchWithTimeout';

export default async function noteTodoApi(description) {
  const response = await fetchWithTimeout(
    'note-todo',
    'POST',
    JSON.stringify({ description })
  );
  try {
    if (response.status === 201) {
      // console.info(`Todo "${description}" added.`);
    } else {
      console.error(
        'Todo could not be added. – Status Code ' + response.status
      );
    }
  } catch {
    console.error('Server not reachable.');
  }

  return await response.json();
}
