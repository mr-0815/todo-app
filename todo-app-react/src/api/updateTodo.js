'use strict';

import { fetchWithTimeout } from './fetchWithTimeout';

export default async function updateTodoApi(todo) {
  const { id } = todo;

  const response = await fetchWithTimeout(
    `edit-todo/${id}`,
    'POST',
    JSON.stringify(todo)
  );
  if (response && response.status === 200) {
    console.info('Todo changed.');
  } else {
    console.error('Something went wrong.');
  }

  return response.status;
}
