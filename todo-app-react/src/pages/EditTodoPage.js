import React, { useContext } from 'react';
import { useParams } from 'react-router-dom';
import ErrorMessage from '../shared/ErrorMessage';
import TodoContext from '../contexts/TodoContext';
import EditTodo from '../components/EditTodo';
import IsLoadingSpinner from '../shared/IsLoadingSpinner';

function EditTodoPage() {
  const { todos } = useContext(TodoContext);
  const id = useParams().id;

  const isValidUUID = (id) => {
    const pattern =
      /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i;
    return pattern.test(id);
  };

  if (!isValidUUID(id)) {
    return <ErrorMessage msg="404 – Not Found" />;
  }

  const todo = todos.filter((todo) => todo.id === id)[0];

  if (!todo) {
    return (
      <div className="mt-36 w-full text-center">
        <IsLoadingSpinner />;
      </div>
    );
  }

  return <EditTodo todo={todo} />;
}

export default EditTodoPage;
