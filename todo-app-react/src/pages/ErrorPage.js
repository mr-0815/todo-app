import React from 'react';
import ErrorMessage from '../shared/ErrorMessage';
import PropTypes from 'prop-types';

function ErrorPage({ msg }) {
  return (
    <>
      <ErrorMessage msg={msg} />
    </>
  );
}

ErrorPage.propTypes = {
  msg: PropTypes.string,
};

export default ErrorPage;
