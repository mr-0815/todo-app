import React, { useContext, useEffect } from 'react';
import TodoItem from './TodoItem';
import TodoContext from '../contexts/TodoContext';
import DisplayItemCount from './DisplayItemCount';
import IsLoadingSpinner from '../shared/IsLoadingSpinner';

function ListTodos() {
  const { todos, isLoading, setIsListPage } = useContext(TodoContext);

  useEffect(() => {
    setIsListPage(true);
  }, []);

  if (isLoading) {
    return <IsLoadingSpinner />;
  }

  if (!todos || !todos.length) {
    return (
      <div>
        <p>You have no open Todos.</p>
      </div>
    );
  }

  return (
    <>
      <div className="flex-auto border rounded-lg">
        <table className="table table-fixed w-full">
          <thead>
            <tr className="h-8">
              <th className="overflow-hidden whitespace-nowrap">Todo</th>
              <th className="w-[25px]"></th>
              <th>
                <span className="pl-2">Status</span>
              </th>
              <th className="w-[112px] pl-0 text-center">Actions</th>
            </tr>
          </thead>
          <tbody>
            {todos.map((todo) => (
              <TodoItem key={todo.id} todoItem={todo} />
            ))}
          </tbody>
        </table>
        <DisplayItemCount />
      </div>
    </>
  );
}

export default ListTodos;
