import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import FetchAgainFromServer from './FetchAgainFromServer';
import TodoContext from '../contexts/TodoContext';
import SettingsMenu from './SettingsMenu';

function Header({ title }) {
  const { isListPage } = useContext(TodoContext);

  return (
    <header className="navbar fixed z-50 mr-container-padding bg-primary text-primary-content shadow-md">
      <div className="flex-1">
        <h1 className="font-semibold text-xl pl-2">{title}</h1>
      </div>
      {isListPage && (
        <div className="flex-none">
          <FetchAgainFromServer />
        </div>
      )}
      <div className="flex-none">
        <SettingsMenu />
      </div>
    </header>
  );
}

Header.propTypes = {
  title: PropTypes.string,
};

export default Header;
