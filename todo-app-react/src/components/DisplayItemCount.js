import React, { useContext } from 'react';
import TodoContext from '../contexts/TodoContext';

function DisplayItemCount() {
  const { todos } = useContext(TodoContext);

  return (
    <div className="mr-itemCount rounded-lg rounded-t-none">
      <div className="py-1 px-4 flex">
        <span className="flex-auto font-semibold uppercase">
          <span>{todos.length}</span> Item
          {todos.length > 1 ? 's' : null}
        </span>
      </div>
    </div>
  );
}

export default DisplayItemCount;
