import React, { useState, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import TodoContext from '../contexts/TodoContext';
import Trashcan from '../shared/Trashcan';
import CheckIcon from '../shared/CheckIcon';

function EditTodo({ todo }) {
  const { editTodo, deleteTodo } = useContext(TodoContext);
  const navigate = useNavigate();

  const id = todo.id;
  const [description, setDescription] = useState(todo.description);
  const [status, setStatus] = useState(todo.status);
  const [additionalInfo, setAddInfo] = useState(todo.additionalInfo);
  const created = new Date(todo.created).toLocaleString();
  const lastModified = new Date(todo.lastModified).toLocaleString();

  const handleSubmit = async (event) => {
    event.preventDefault();
    const updTodo = {
      id,
      description,
      status,
      additionalInfo,
    };

    if (isNoChanges() === true) {
      console.info('No changes made.');
      navigate('/');
    } else {
      if (updTodoIsValid() === false) {
        console.error('Missing items.');
        navigate('/');
      } else {
        await editTodo(updTodo);

        // visualize success and return home
        document.getElementById('swapCheckButton').classList.add('btn-ghost');
        document.getElementById('swapCheckElem').classList.add('swap-active');
        document
          .getElementById('checkItemElem')
          .classList.add('transition-transform', 'fill-primary', 'scale-[30]');
        setTimeout(() => {
          navigate('/');
        }, 900);
      }
    }
  };

  const isNoChanges = () => {
    return (
      id === todo.id &&
      description === todo.description &&
      status === todo.status &&
      additionalInfo === todo.additionalInfo
    );
  };

  const updTodoIsValid = () => {
    return !(!id || !description || !status);
  };

  const handleDelete = async (event) => {
    event.preventDefault();
    if (confirm('Delete this item?')) {
      navigate('/');
      await deleteTodo(id);
    }
  };

  const handleCancel = async (event) => {
    event.preventDefault();
    navigate('/');
  };

  return (
    <div className="flex flex-col w-full">
      <h3 className="font-semibold text-xl mt-4 mb-2 pl-2">Edit Todo</h3>
      <form
        onSubmit={(event) => {
          handleSubmit(event);
        }}
        className="form-control"
      >
        {/* Description  */}
        <input
          type="text"
          value={description}
          onChange={(event) => {
            setDescription(event.target.value);
          }}
          className="input input-bordered input-primary"
        />
        {/* Status */}
        <div className="flex my-3 sm:my-6">
          <label className="label flex-0 mr-2">Status</label>
          <select
            value={status}
            onChange={(event) => {
              setStatus(event.target.value);
            }}
            className="select select-bordered select-primary flex-auto"
          >
            <option value="open">open</option>
            <option value="in Progress">in Progress</option>
            <option value="done">done</option>
          </select>
        </div>
        {/* Additional Info */}
        <textarea
          label="additionalInfo"
          placeholder={additionalInfo || 'Additional Information'}
          value={additionalInfo || ''}
          rows="5"
          onChange={(event) => {
            setAddInfo(event.target.value);
          }}
          className="textarea textarea-bordered textarea-primary rounded-lg mb-3 sm:mb-6"
        ></textarea>

        {/* Buttons  */}
        {/* Apply Changes  */}
        <div className="flex flex-col sm:flex-row">
          <button
            type="submit"
            className="btn btn-primary flex-1 z-50"
            id="swapCheckButton"
          >
            <div className="swap swap-active" id="swapCheckElem">
              <span className="swap-off self-center">Apply Changes</span>
              <span className="swap-on mx-auto">
                <CheckIcon innerId="checkItemElem" />
              </span>
            </div>
          </button>

          {/* Delete */}
          <button
            onClick={(event) => {
              handleDelete(event);
            }}
            className="btn btn-outline btn-error flex-0 fill-error hover:fill-error-content my-3 sm:my-0 sm:mx-3"
          >
            <Trashcan />
          </button>

          {/* Cancel  */}
          <button
            onClick={(event) => {
              handleCancel(event);
            }}
            className="btn btn-primary hover:btn flex-1"
          >
            Cancel
          </button>
        </div>
      </form>

      {/* Show Debug Info  */}
      <div
        className="collapse collapse-arrow mt-3 sm:mt-6 mb-16 px-2 rounded-lg bg-base-200"
        tabIndex={0}
      >
        <div className="collapse-title px-4 text-center text-sm font-semibold uppercase">
          <p>Show More Information</p>
        </div>

        <div className="collapse-content">
          <table className="mx-auto text-sm">
            <tbody>
              <tr>
                <td colSpan={2}>
                  ID: &nbsp; <span className="font-mono text-xs">{id}</span>
                </td>
              </tr>
              <tr>
                <td>Created:</td>
                <td className="text-right font-mono text-xs">{created}</td>
              </tr>
              <tr>
                <td>Last Modified:</td>
                <td className="text-right font-mono text-xs">{lastModified}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

EditTodo.propTypes = {
  todo: PropTypes.object,
};

export default EditTodo;
