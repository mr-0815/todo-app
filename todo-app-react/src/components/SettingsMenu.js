import React from 'react';
import HamburgerIcon from '../shared/HamburgerIcon';
import DarkModeToggle from './DarkModeToggle';

function SettingsMenu() {
  return (
    <div className="dropdown dropdown-end">
      <label tabIndex="0" className="btn btn-primary btn-circle">
        <HamburgerIcon />
      </label>
      <ul
        tabIndex="0"
        className="dropdown-content menu p-1 shadow bg-base-100 dark:text-primary rounded-box min-w-fit whitespace-nowrap"
      >
        <li>
          <DarkModeToggle />
        </li>
        <li>
          <a>About</a>
        </li>
      </ul>
    </div>
  );
}

export default SettingsMenu;
