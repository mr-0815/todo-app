import React, { useContext, useState } from 'react';
import TodoContext from '../contexts/TodoContext';
import getTodosApi from '../api/getTodos';
import CheckIcon from '../shared/CheckIcon';
import ReloadIcon from '../shared/ReloadIcon';

function FetchAgainFromServer() {
  const { setTodos, setIsLoading } = useContext(TodoContext);
  const [onClickDisabled, setOnClickDisabled] = useState(false);

  const handleClickForFetch = async () => {
    if (!onClickDisabled) {
      setIsLoading(true);
      const todos = await getTodosApi();
      setTodos(todos);
      setIsLoading(false);

      setOnClickDisabled(true);
      const swapElem = document.getElementById('swapReload');
      swapElem.classList.add('swap-active');
      setTimeout(() => {
        swapElem.classList.remove('swap-active');
        setOnClickDisabled(false);
      }, 3_000);
    }
  };

  return (
    <div
      className="tooltip tooltip-left z-50"
      data-tip="Fetch recent data from server."
    >
      <label
        className="swap swap-rotate btn btn-primary btn-circle"
        id="swapReload"
        onClick={handleClickForFetch}
      >
        <div className="swap-off">
          <ReloadIcon />
        </div>
        <div className="swap-on">
          <CheckIcon />
        </div>
      </label>
    </div>
  );
}

export default FetchAgainFromServer;
