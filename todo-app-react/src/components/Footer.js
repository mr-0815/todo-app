import React from 'react';

function Footer() {
  return (
    <div className="footer fixed left-auto bottom-0 bg-primary text-primary-content shadow-inner">
      <p className="mr-container-padding py-3">A Simple Todo App.</p>
    </div>
  );
}

export default Footer;
