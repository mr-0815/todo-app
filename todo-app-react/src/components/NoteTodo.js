import React, { useState, useContext } from 'react';
import TodoContext from '../contexts/TodoContext';
import { useNotification } from '../contexts/NotificationContext';

function NoteTodo() {
  const [todo, setTodo] = useState('');

  const { noteTodo, isLoading } = useContext(TodoContext);

  const dispatch = useNotification();
  const handleNewNotification = (alertType, message) => {
    dispatch({
      alertType,
      message,
    });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (!todo) {
      // @todo – ask for input
      return;
    }
    await noteTodo(todo);
    setTodo('');
    handleNewNotification('success', `Todo "${todo}" added.`);
  };

  return (
    <div className="flex-none mt-4 mb-4">
      <form onSubmit={handleSubmit} className="form-control">
        <input
          type="text"
          placeholder="What do you want to do?"
          value={todo}
          onChange={(event) => {
            setTodo(event.target.value);
          }}
          className="input input-primary input-bordered focus:outline-none focus:border-2"
          autoFocus
        />
        <button
          type="submit"
          className="btn btn-primary hover:btn-primary my-1"
          disabled={isLoading}
        >
          Note Todo
        </button>
      </form>
    </div>
  );
}

export default NoteTodo;
