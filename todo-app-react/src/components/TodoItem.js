import React, { useState, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import TodoContext from '../contexts/TodoContext';
import Trashcan from '../shared/Trashcan';
import EditIcon from '../shared/EditIcon';
import CheckIcon from '../shared/CheckIcon';
import CancelIcon from '../shared/CancelIcon';
import HasAttachment from '../shared/HasAttachment';

function TodoItem({ todoItem }) {
  const [description, setDescription] = useState(todoItem.description);
  const [status, setStatus] = useState(todoItem.status);
  const [isInlineEditMode, setInlineEditMode] = useState(false);

  const { editTodo, deleteTodo, setIsListPage } = useContext(TodoContext);
  const navigate = useNavigate();

  const handleSubmit = (event) => {
    event.preventDefault();
    const statusNew =
      event.target.type === 'select-one' && event.target.value
        ? event.target.value
        : status;
    editTodo({
      id: todoItem.id,
      description,
      status: statusNew,
    });
    document.querySelector('tr.active')?.classList.remove('active');
    setInlineEditMode(false);
  };

  const handleDelete = async () => {
    await deleteTodo(todoItem.id);
  };

  const handleEditClick = () => {
    setIsListPage(false);
    navigate(`/edit/${todoItem.id}`);
  };

  const handleInlineEdit = (event) => {
    setInlineEditMode(true);
    event.target.parentElement.classList.add('active');
  };

  const handleCancelInlineEdit = () => {
    setDescription(todoItem.description);
    setStatus(todoItem.status);
    document.querySelector('tr.active').classList.remove('active');
    setInlineEditMode(false);
  };

  return (
    <tr
      onDoubleClick={(event) => {
        handleInlineEdit(event);
      }}
      className={status !== 'done' ? 'hover' : ''}
    >
      {!isInlineEditMode ? (
        <>
          {/* default display, if not in edit mode  */}
          <td
            className={`overflow-hidden whitespace-nowrap text-ellipsis ${
              status === 'done'
                ? 'text-base-300 dark:text-base-content dark:text-opacity-60'
                : ''
            }`}
          >
            {description}
          </td>
          <td className="text-right pt-3 px-0">
            {todoItem.additionalInfo && (
              <>
                <div
                  className="tooltip z-50"
                  data-tip="This item has additional information added."
                >
                  <HasAttachment className="inline opacity-60" />
                </div>
              </>
            )}
          </td>
          <td className="form-control">
            <select
              value={status}
              onChange={(event) => {
                setStatus(event.target.value);
                handleSubmit(event);
              }}
              className="select select-bordered hover:select-primary hover:focus:outline-none select-sm h-[30px]"
            >
              <option value="open">open</option>
              <option value="in Progress">in Progress</option>
              <option value="done">done</option>
            </select>
          </td>
          <td className="pl-0 pr-4">
            <button
              onClick={handleEditClick}
              className="btn btn-outline hover:btn-primary btn-sm mr-1"
            >
              <EditIcon />
            </button>
            <button
              onClick={handleDelete}
              className="btn btn-outline btn-sm hover:btn-error fill-current"
            >
              <Trashcan />
            </button>
          </td>
        </>
      ) : (
        <>
          {/* inline edit mode  */}
          <td colSpan={2}>
            <form onSubmit={(event) => handleSubmit(event)}>
              <input
                type="text"
                value={description}
                onChange={(event) => {
                  setDescription(event.target.value);
                }}
                className="input input-primary input-sm w-full"
                autoFocus
              />
            </form>
          </td>
          <td className="form-control">
            <select
              value={status}
              onChange={(event) => {
                setStatus(event.target.value);
              }}
              className="select select-primary select-sm h-[30px]"
            >
              <option value="open">open</option>
              <option value="in Progress">in Progress</option>
              <option value="done">done</option>
            </select>
          </td>
          <td className="pl-0 pr-4">
            <button
              type="submit"
              onClick={(event) => handleSubmit(event)}
              className="btn btn-outline btn-primary fill-primary hover:fill-base-content dark:hover:fill-primary-content dark:hover:btn-primary btn-sm mr-1 "
            >
              <CheckIcon />
            </button>
            <button
              onClick={handleCancelInlineEdit}
              className="btn btn-outline btn-error btn-sm"
            >
              <CancelIcon />
            </button>
          </td>
        </>
      )}
    </tr>
  );
}

TodoItem.propTypes = {
  todoItem: PropTypes.object.isRequired,
};

export default TodoItem;
