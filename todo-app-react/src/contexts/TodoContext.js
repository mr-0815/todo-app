import React, { createContext, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import deleteTodoApi from '../api/deleteTodo';
import getTodosApi from '../api/getTodos';
import noteTodoApi from '../api/noteTodo';
import updateTodoApi from '../api/updateTodo';

const TodoContext = createContext();

export const TodoProvider = ({ children }) => {
  const [todos, setTodos] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isListPage, setIsListPage] = useState(false);

  // fetch initial Todos from API
  useEffect(() => {
    (async () => {
      const todos = await getTodosApi();
      setTodos(todos);
      setIsLoading(false);
    })();
  }, []);

  const noteTodo = async (todo) => {
    const newTodo = await noteTodoApi(todo);
    newTodo && setTodos(!todos.length ? [newTodo] : [...todos, newTodo]);
  };

  const editTodo = async (updTodo) => {
    await updateTodoApi(updTodo);
    setTodos(
      todos.map((todo) =>
        todo.id === updTodo.id ? { ...todo, ...updTodo } : todo
      )
    );
  };

  const deleteTodo = async (id) => {
    await deleteTodoApi(id);
    setTodos(todos.filter((todo) => todo.id !== id));
  };

  return (
    <TodoContext.Provider
      value={{
        todos,
        noteTodo,
        editTodo,
        deleteTodo,
        isLoading,
        setIsLoading,
        setTodos,
        isListPage,
        setIsListPage,
      }}
    >
      {children}
    </TodoContext.Provider>
  );
};

TodoProvider.propTypes = {
  children: PropTypes.node,
};

export default TodoContext;
