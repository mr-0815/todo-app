import React, { createContext, useState, useEffect } from 'react';
import PropTypes from 'prop-types';

const AppContext = createContext();

// DARK MODE & LIGHT MODE

export const AppProvider = ({ children }) => {
  const [isDarkMode, setDarkMode] = useState(false);

  // check for preferences
  useEffect(() => {
    if (
      localStorage.theme === 'dark' ||
      (!('theme' in localStorage) &&
        window.matchMedia('(prefers-color-scheme: dark)').matches)
    ) {
      setDarkMode(true);
    }
  }, []);

  // change appearance, if isDarkMode changes
  useEffect(() => {
    const rootElem = document.getElementById('root');
    if (isDarkMode) {
      rootElem.setAttribute('data-theme', 'mr-dark');
      rootElem.classList.add('dark');
      localStorage.theme = 'dark';
    } else {
      rootElem.setAttribute('data-theme', 'mr-light');
      rootElem.classList.remove('dark');
      localStorage.theme = 'light';
    }
  }, [isDarkMode]);

  return (
    <AppContext.Provider
      value={{
        isDarkMode,
        setDarkMode,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

AppProvider.propTypes = {
  children: PropTypes.node,
};

export default AppContext;
