// CREDITS:
// https://www.youtube.com/watch?v=KYKmqeU6lOI
// https://github.com/daryanka/notification-component

import React, { createContext, useContext, useReducer } from 'react';
import PropTypes from 'prop-types';
import { v4 } from 'uuid';
import Notification from '../shared/Notification';

const NotificationContext = createContext();

export const NotificationProvider = (props) => {
  const [state, dispatch] = useReducer((state, action) => {
    switch (action.type) {
      case 'ADD_NOTIFICATION':
        return [...state, { ...action.payload }];
      case 'REMOVE_NOTIFICATION':
        return state.filter((el) => el.id !== action.id);
      default:
        return state;
    }
  }, []);

  return (
    <NotificationContext.Provider value={dispatch}>
      <div className="fixed z-40 mr-container-padding top-16 my-4 w-full">
        {state.map((note) => {
          return <Notification dispatch={dispatch} key={note.id} {...note} />;
        })}
      </div>
      {props.children}
    </NotificationContext.Provider>
  );
};

export const useNotification = () => {
  const dispatch = useContext(NotificationContext);

  return (props) => {
    dispatch({
      type: 'ADD_NOTIFICATION',
      payload: {
        id: v4(),
        ...props,
      },
    });
  };
};

NotificationProvider.propTypes = {
  children: PropTypes.node,
};

export default NotificationProvider;
